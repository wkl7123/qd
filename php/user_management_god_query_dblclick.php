<?php 
	error_reporting(E_ERROR| E_PARSE);
	header("Content-Type:text/html;charset=UTF-8");
	$uid=$_GET['uid'];
	include 'translate.php';
	include 'connect_mysql.php';
	$connection->query("SET NAMES 'UTF8'");
	$question="select nickname,wechat_id,register_time,name,sex,mail,phone,city,area,address,remark,id_card,card_photo from user where uid=$uid";
	$result=$connection->query($question);
	$row = $result->fetch_assoc();
 ?>


 <!DOCTYPE html>
 <html>
 <head>
 	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" >
 	<script type="text/javascript" src="../js/jquery-2.1.1.min.js"></script>
 	<title>anecdote_announcement</title>
 	<style type="text/css">
 		img{
 			width:200px;
 			height:200px;
 			margin:15px;
 		}
 		table{
 			margin:10px auto;
 			font-size:14px;
 			border-collapse:collapse;
 			font-family: 'hiragino sans gb','Josefin Sans','helvetica neue', tahoma;
 			line-height:16px;
 		}
 		table td{
 			border:1px solid #3393b0;
 		}
 		textarea{
 			width:95%;
 			vertical-align: middle;
 			margin:0 auto;
 		}
		.btn_wkl {
			-webkit-border-radius: 0;
			-moz-border-radius: 0;
			border-radius: 0px;
			font-family: Arial;
			color: #6bb3bd;
			background: #ffffff;
			border: dotted #3393b0 2px;
			text-decoration: none;
		}
		.btn_wkl:hover {
			background: #6bb3bd;
			text-decoration: none;
			color:#fff;
		}
		#submit{
			display: block;
			width:100px;
			margin-left:340px;
			font-size: 20px;
			padding-left:25px;
			padding-top:10px;
			padding-bottom:10px;
		}
		h1{
			font-size:20px;
			margin-left:15px;
			margin-top:10px;
			margin-bottom:10px;
			line-height: 20px;
		}
 	</style>
 	<script type="text/javascript">
 		function modify(){
 			var remark=document.getElementById("remark").value;
 			remark=remark.replace(/'/g,"\\'");
 			var data={"remark":encodeURI(remark),"uid":<?php echo $uid ?>};
 			var req=$.ajax({
				url:"modify_user_remark.php",
				method: "POST",
				data:data,
				dataType:"text"
			});
			req.done(function(message){
				if(message="ok")
					alert("修改成功"+message);
				else
					alert("修改失败");
			});
			req.fail(function(){
				alert("error!");
			});
		 }
 	</script>
 </head>
 <body>
 	<h1>上帝个人信息表</h1>
 	<table>
 		<tr>
 			<td>用户ID</td>
 			<td><?php echo $uid ?></td>
 			<td>身份证号: <?php echo $row['id_card'] ?> </td>
 		</tr>
 		<tr>
 			<td>昵称</td>
 			<td><?php echo $row['nickname'] ?></td>
 			<td rowspan="10" style="width:235px;"> <img src=<?php echo "'".$row['card_photo']."'"?> alt="身份证照片"/> </td>
 		</tr>
 		<tr>
 			<td>微信ID</td>
 			<td><?php echo $row['wechat_id'] ?></td>
 		</tr>
 		<tr>
 			<td>注册时间</td>
 			<td><?php echo $row['register_time'] ?></td>
 		</tr>
 		<tr>
 			<td>姓名</td>
 			<td><?php echo $row['name'] ?></td>
 		</tr>
 		<tr>
 			<td>性别</td>
 			<td><?php echo translate($row['sex']) ?></td>
 		</tr>
 		<tr>
 			<td>邮箱</td>
 			<td><?php echo $row['mail'] ?></td>
 		</tr>
 		<tr>
 			<td>手机</td>
 			<td><?php echo $row['phone']; ?></td>
 		</tr>
 		<tr>
 			<td>城市</td>
 			<td><?php echo translate($row['city']); ?></td>
 		</tr>
 		<tr>
 			<td>区域</td>
 			<td><?php echo $row['area'] ?></td>
 		</tr>
 		<tr>
 			<td>详细地址</td>
 			<td><?php echo $row['address'] ?></td>
 		</tr>
 		<tr>
 			<td>备注</td>
 			<td colspan="2"><textarea id="remark"><?php echo $row['remark'] ?></textarea></td>
 		</tr>
 	</table>
 	<a href="#" class="btn_wkl" id="submit" onclick="modify();return false;">提交修改</a>
 </body>
 </html>