<?php 
	error_reporting(E_ERROR| E_PARSE);
	header("Content-Type:text/html;charset=UTF-8");
	$n=$_GET['n'];
	include 'connect_mysql.php';
	$connection->query("SET NAMES 'UTF8'");
	$question="select n,text,time,photo1,photo2,photo3,photo4,photo5,photo6 from anecdote_public where n=$n";
	$result=$connection->query($question);
	$row = $result->fetch_assoc();
	$n=$row['n'];
	$text=$row['text'];
	$photo1=$row['photo1'];
	$photo2=$row['photo2'];
	$photo3=$row['photo3'];
	$photo4=$row['photo4'];
	$photo5=$row['photo5'];
	$photo6=$row['photo6'];
	$time  =$row['time'];
 ?>
<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<script src="../js/jquery-2.1.1.min.js"></script>
		<script type="text/javascript" src="../js/jquery.form.js"></script> 
		<title></title>
		<style>
			body{
				position: absolute;
				width:95%;
				background-color: white;
			}
			#wrap{
				/*margin: 0 auto;*/
				margin-left:55px;
				width:456px;
				/*border:1px dashed #6bb3bd;*/
			}
			#say{
				width:455px;
				height:104px;
				font-size:20px;
				font-family:"Microsoft YaHei",微软雅黑,"MicrosoftJhengHei",华文细黑,STHeiti,MingLiu;
				display: block;
				margin: 0 auto;
				margin-top: 20px;
			}
			#photoBox{
				font-size: 0;
				margin-top:20px;
				width:456px;
				height:304px;
				padding: 0px;
			}
			.photo{
				width: 150px;
				height:150px;
				border:1px solid rgba(255,255,255,0);
				z-index: -3;
			}
			#submit{
				/*position: relative;*/
				/*top:130px;
				left:100px;*/
				display: inline-block;
				font-size:20px;
				padding: 15px 20px 18px 20px;
				margin-left:330px;
				margin-top:20px;
			}
			.banner-button:hover {
				background-color:#DFCB82;
			}

			.btn_wkl {
				-webkit-border-radius: 0;
				-moz-border-radius: 0;
				border-radius: 0px;
				font-family: Arial;
				color: #6bb3bd;
				background: #ffffff;
				border: dotted #3393b0 2px;
				text-decoration: none;
			}

			.btn_wkl:hover {
				background: #6bb3bd;
				text-decoration: none;
				color:#fff;
			}
			area{
				cursor:pointer;
			}
		</style>
		<script>
			function changeIMG(obj){
				IMAGE=obj;
				document.getElementById('fileupload').click();
			}
			function deleteIMG(photo_id){
				document.getElementById(photo_id).src="http://localhost/quding/server/photos/0.png";
				// alert(document.getElementById('photo2').src);
			}

			function modify(){
				var text=document.getElementById('say').value;
				text=text.replace(/'/g,"\\'");
				var photo1=document.getElementById('photo1').src;
				var photo2=document.getElementById('photo2').src;
				// alert("photo2:"+photo2);
				var photo3=document.getElementById('photo3').src;
				var photo4=document.getElementById('photo4').src;
				var photo5=document.getElementById('photo5').src;
				var photo6=document.getElementById('photo6').src;
				if(photo1.split(".")[(photo1.split(".")).length-1]=="html"){photo1="";}
				if(photo2.split(".")[(photo2.split(".")).length-1]=="html"){photo2="";}
				if(photo3.split(".")[(photo3.split(".")).length-1]=="html"){photo3="";}
				if(photo4.split(".")[(photo4.split(".")).length-1]=="html"){photo4="";}
				if(photo5.split(".")[(photo5.split(".")).length-1]=="html"){photo5="";}
				if(photo6.split(".")[(photo6.split(".")).length-1]=="html"){photo6="";}
				var data={n:<?php echo $n ?>,text:encodeURI(text),photo1:photo1,photo2:photo2,photo3:photo3,photo4:photo4,photo5:photo5,photo6:photo6};
				var req=$.ajax({
					url:"../php/anecdote_announcement_modify.php",
					method: "POST",
					data:data,
					contentType:'application/x-www-form-urlencoded; charset=UTF-8',
					dataType:"json"
				});
				req.done(function(data){
					data=eval(data);
					if(data!='fail'){
						alert("修改成功!"+data.value);
					}else{
						alert("fail");
					}
					// window.location.href="anecdote_announcement.html";
				});
				req.fail(function(){
						alert("error!");
				});
			}
		</script>
	</head>
	<body>
	<!-- http://css3buttongenerator.com/ -->
		<!-- <a class="btn_wkl" id="return" href="javascript:document.location.href='anecdote_announcement.html'" target="_top">&larr;</a> -->
		<div id="wrap">
			<textarea name="say" id="say" cols="44" rows="4" autofocus><?php echo $text ?></textarea>
			<div id="photoBox">
				<img src=<?php echo "'$photo1'" ?> alt="eva1" class="photo" id="photo1" usemap="#photo-area1" onclick="changeIMG(this)">
				<img src=<?php echo "'$photo2'" ?> alt="eva2" class="photo" id="photo2" usemap="#photo-area2" onclick="changeIMG(this)">
				<img src=<?php echo "'$photo3'" ?> alt="eva3" class="photo" id="photo3" usemap="#photo-area3" onclick="changeIMG(this)">
				<img src=<?php echo "'$photo4'" ?> alt="eva4" class="photo" id="photo4" usemap="#photo-area4" onclick="changeIMG(this)">
				<img src=<?php echo "'$photo5'" ?> alt="eva5" class="photo" id="photo5" usemap="#photo-area5" onclick="changeIMG(this)">
				<img src=<?php echo "'$photo6'" ?> alt="eva6" class="photo" id="photo6" usemap="#photo-area6" onclick="changeIMG(this)">
				<map name="photo-area1" id="photo-area1" onclick="deleteIMG('photo1')">
					<area shape="rect" coords="100,0,150,50"  />
				</map>
				<map name="photo-area2" id="photo-area2" onclick="deleteIMG('photo2')">
					<area shape="rect" coords="100,0,150,50"  />
				</map>
				<map name="photo-area3" id="photo-area3" onclick="deleteIMG('photo3')">
					<area shape="rect" coords="100,0,150,50"  />
				</map>
				<map name="photo-area4" id="photo-area4" onclick="deleteIMG('photo4')">
					<area shape="rect" coords="100,0,150,50"  />
				</map>
				<map name="photo-area5" id="photo-area5" onclick="deleteIMG('photo5')">
					<area shape="rect" coords="100,0,150,50"  />
				</map>
				<map name="photo-area6" id="photo-area6" onclick="deleteIMG('photo6')">
					<area shape="rect" coords="100,0,150,50"  />
				</map>
			</div>
			<a class='btn_wkl' id='submit' href='#' onclick='modify()'>提交修改</a>
			<!-- <input type="file" name="file" id="file" onclick="changeIMG()" style="display: none;" /> -->

			<input id="fileupload" type="file" name="mypic" style="position: fixed;display: none;"> 
<!-- 			<div class="progress"> 
				<span class="bar"></span><span class="percent">0%</span > 
			</div> 
			<div class="files"></div> 
			<div id="showimg"></div>  -->
		</div>
	</body>
<script type="text/javascript">
	$(function () { 
		IMAGE=""; 
	    $("#fileupload").wrap("<form id='myupload' action='../php/action.php' method='post' enctype='multipart/form-data'></form>"); 
	    $("#fileupload").change(function(){ //选择文件 
	        $("#myupload").ajaxSubmit({ 
	            dataType:  'json', //数据格式为json 
	            success: function(data) { //成功 
	                if(data.pic!=null)
	                {
		                var img = "../server/photos/"+data.pic; 
		                IMAGE.src=img;   // if img is null?
		            }
	            }
	        })
	    })
    })
</script>
</html>