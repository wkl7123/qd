<?php 
include 'thumbnail.php';
$action = $_GET['act']; 
if($action=='delimg'){ //删除图片 
    $filename = $_POST['imagename']; 
    if(!empty($filename)){ 
        unlink('files/'.$filename); 
        echo '1'; 
    }else{ 
        echo '删除失败.'; 
    } 
}else{ //上传图片 
    $picname = $_FILES['mypic']['name']; 
    $picsize = $_FILES['mypic']['size']; 
    if ($picname != "") { 
        if ($picsize > 1024000) { //限制上传大小 
            echo '图片大小不能超过1024k'; 
            exit; 
        } 
        $type = strrchr($picname, '.'); //限制上传格式 
        if ($type != ".gif" && $type != ".jpg" && $type!= ".png" && $type!= ".jpeg" && $type != ".GIF"&& $type != ".JPG" && $type!= ".PNG" && $type!= ".JPEG") { 
            echo '图片格式不对！'; 
            exit; 
        } 
        $rand = rand(100, 999); 
        $date = date("YmdHis");
        $pics = $date . $rand . $type; //命名图片名称 
        $minipics=$date . $rand . ".png"; //命名图片名称 
        //上传路径 
        $pic_path    =$_SERVER['DOCUMENT_ROOT']."/quding/server/photos/". $pics; 
        move_uploaded_file($_FILES['mypic']['tmp_name'], $pic_path); 
        $minipic_path=$_SERVER['DOCUMENT_ROOT']."/quding/server/miniphotos/". $minipics;
        CreateThumbnail($pic_path,100,100,$minipic_path);
    } 
    $size = round($picsize/1024,2); //转换成kb 
    $arr = array( 
        'name'=>$picname, 
        'pic'=>$pics, 
        'size'=>$size 
    ); 
    echo json_encode($arr); //输出json数据 
} 
?>