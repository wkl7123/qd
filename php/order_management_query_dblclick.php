<?php 
	error_reporting(E_ERROR| E_PARSE);
	header("Content-Type:text/html;charset=UTF-8");
	$oid=$_GET['oid'];
	include 'translate.php';
	include 'connect_mysql.php';
	$connection->query("SET NAMES 'UTF8'");
	$question="select uid_god,time1,angels_num,uid_angel,time2,price,time3,pay_record_god,state1,pay_record_staff,state2,alipay_god,alipay_angel,tracking_number,remark from ording where oid=$oid";
	$result=$connection->query($question);
	$row = $result->fetch_assoc();
	$tmp=$row['state1'];
	echo "<script>STATE='$tmp';</script>"
 ?>


 <!DOCTYPE html>
 <html>
 <head>
 	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" >
 	<script type="text/javascript" src="../js/jquery-2.1.1.min.js"></script>
 	<title>anecdote_announcement</title>
 	<style type="text/css">
 		img{
 			width:200px;
 			height:200px;
 			margin:15px;
 		}
 		table{
 			margin:10px auto;
 			font-size:14px;
 			border-collapse:collapse;
 			font-family: 'hiragino sans gb','Josefin Sans','helvetica neue', tahoma;
 			line-height:20px;
 		}
 		table td{
 			border:1px solid #3393b0;
 			width:195px;
 		}
 		textarea{
 			width:95%;
 			vertical-align: middle;
 			margin:0 auto;
 		}
		.btn_wkl {
			-webkit-border-radius: 0;
			-moz-border-radius: 0;
			border-radius: 0px;
			font-family: Arial;
			color: #6bb3bd;
			background: #ffffff;
			border: dotted #3393b0 2px;
			text-decoration: none;
		}
		.btn_wkl:hover {
			background: #6bb3bd;
			text-decoration: none;
			color:#fff;
		}
		#submit{
			position: absolute;
			display: block;
			width:100px;
			margin-left:275px;
			font-size: 20px;
			padding-left:25px;
			padding-top:10px;
			padding-bottom:10px;
		}
		#cancel_order{
			position: absolute;
			margin:10px;
			display: none;
		}
		#pay_record_staff{
			width:95%;
		}
 	</style>
 	<script type="text/javascript">
 		function modify(){
 			var remark=document.getElementById("remark").value;
 			var pay_record_staff=document.getElementById('pay_record_staff').value;
 			var state2=document.getElementById('state2').value;
 			remark=remark.replace(/'/g,"\\'");
 			var data={"remark":encodeURI(remark),"oid":<?php echo $oid ?>,"pay_record_staff":pay_record_staff,"state2":state2};
 			var req=$.ajax({
				url:"modify_ording.php",
				method: "POST",
				data:data,
				dataType:"text"
			});
			req.done(function(message){
				if(message=="ok")
					alert("修改成功,请自行刷新外部表格");
				else
					alert(message);
			});
			req.fail(function(){
				alert("error!");
			});
		 }

		 function cancel_order(){
		 	var tmp=confirm("确定要中止订单吗");
		 	if(tmp){
		 		var data={"oid":<?php echo $oid ?>};
			 	var req=$.ajax({
			 		url:"orderCancelStaff.php",
			 		method:"POST",
			 		data:data,
			 		dataType:"text"
			 	});
			 	req.done(function(message){
			 		if(message=="ok")
						alert("中止订单成功,请自行刷新外部表格");
					else
						alert(message);
			 	});
			 	req.fail(function(){
			 		alert("error!");
			 	});
			 }
		 }

		function default_select_value(){
			document.getElementById('state2').value=<?php echo "'".$row['state2']."'" ?>;
			if(STATE=='five' || STATE=='six'){
				$("#cancel_order").css("display","inline-block");
			}
		}
 	</script>
 </head>
 <body onload=" default_select_value()">
 	<table>
 		<tr>
 			<td>订单号</td>
 			<td><?php echo $oid ?></td>
 		</tr>
 		<tr>
 			<td>上帝ID</td>
 			<td><?php echo $row['uid_god'] ?></td>
 		</tr>
 		<tr>
 			<td>下单时间</td>
 			<td><?php echo $row['time1'] ?></td>
 		</tr>
 		<tr>
 			<td>抢单人数</td>
 			<td><?php echo $row['angels_num'] ?></td>
 		</tr>
 		<tr>
 			<td>中单天使ID</td>
 			<td><?php echo $row['uid_angel'] ?></td>
 		</tr>
 		<tr>
 			<td>中单时间</td>
 			<td><?php echo $row['time2'] ?></td>
 		</tr>
 		<tr>
 			<td>中标金额</td>
 			<td><?php echo $row['price'] ?></td>
 		</tr>
 		<tr>
 			<td>支付时间</td>
 			<td><?php echo $row['time3'] ?></td>
 		</tr>
 		<tr>
 			<td>支付记录</td>
 			<td><?php echo $row['pay_record_god'] ?></td>
 		</tr>
 		<tr>
 			<td>上帝支付宝</td>
 			<td><?php echo $row['alipay_god'] ?></td>
 		</tr>
 		<tr>
 			<td>中单天使支付宝</td>
 			<td><?php echo $row['alipay_angel'] ?></td>
 		</tr>
 		<tr>
 			<td>快递单号</td>
 			<td><?php echo $row['tracking_number'] ?></td>
 		</tr>
 		<tr>
 			<td>订单状态</td>
 			<td><?php echo translate($row['state1']) ?></td>
 		</tr>
 		<tr>
 			<td>分配状态</td>
 			<td>
 				<select id="state2">
 					<option value="a">无需分配</option>
 					<option value="b">待支付</option>
 					<option value="c">已支付</option>
 					<option value="d">待退款</option>
 					<option value="e">已付款</option>
 				</select>
 			</td>
 		</tr>
 		<tr>
 			<td>分配支付记录</td>
 			<td><input id="pay_record_staff" type="text" value=<?php echo "'".$row['pay_record_staff']."'" ?>></input></td>
 		</tr>
 		<tr>
 			<td>备注</td>
 			<td colspan="2"><textarea id="remark"><?php echo $row['remark'] ?></textarea></td>
 		</tr>
 	</table>
 	<a href="#" class="btn_wkl" id="cancel_order" onclick="cancel_order();return false;">中止订单</a>
 	<a href="#" class="btn_wkl" id="submit" onclick="modify();return false;">提交修改</a>
 </body>
 </html>