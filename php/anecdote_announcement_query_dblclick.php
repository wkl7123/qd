<?php 
	error_reporting(E_ERROR| E_PARSE);
	header("Content-Type:text/html;charset=UTF-8");
	$n=$_GET['n'];
	include 'connect_mysql.php';
	$connection->query("SET NAMES 'UTF8'");
	$question="select n,text,time,photo1,photo2,photo3,photo4,photo5,photo6 from anecdote_public where n=$n";
	$result=$connection->query($question);
	$row = $result->fetch_assoc();
	$n=$row['n'];
	$text=$row['text'];
	$photo1=$row['photo1'];
	$photo2=$row['photo2'];
	$photo3=$row['photo3'];
	$photo4=$row['photo4'];
	$photo5=$row['photo5'];
	$photo6=$row['photo6'];
	$time  =$row['time'];
	// echo $photo1;
 ?>

 <!DOCTYPE html>
 <html>
 <head>
 	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" >
 	<title>anecdote_announcement</title>
 	<style type="text/css">
 		body{
 			position: absolute;
 			height:400px;
 			width: 400px;
 			/*border:1px solid blue;*/
 		}
 		#text{
 			width: 306px;
 			height:100px;
 			margin:10px auto;
 			border:1px solid black;
 		}
 		.photo{
 			width:100px;
 			height:100px;
 			margin:0px;
 			padding:1px;
 			border:0px;
 		}
 		#photo-wrap{
 			width: 306px;
 			height:204px;
 			margin:0px auto;
 			font-size: 0px;
 			border:1px solid rgba(255,255,255,0);
 		}
 		#time{
 			margin-left:50px;
 			margin-top:30px;
 			font-size: 0.8em;
 		}
 	</style>
 </head>
 <body>
 	<div id="text"><?php echo "$text" ?></div>
 	<div id="photo-wrap">
 		<img src=<?php echo "'$photo1'" ?> class="photo">
 		<img src=<?php echo "'$photo2'" ?> class="photo">
 		<img src=<?php echo "'$photo3'" ?> class="photo">
 		<img src=<?php echo "'$photo4'" ?> class="photo">
 		<img src=<?php echo "'$photo5'" ?> class="photo">
 		<img src=<?php echo "'$photo6'" ?> class="photo">
 	</div>
 	<p id="time">发布时间: <?php echo"$time" ?></p>
 </body>
 </html>