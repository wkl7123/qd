<?php 
	header("Content-Type:text/html;charset=UTF-8");
	function translate($enName){
		switch ($enName) {
			case 'angel':
				return '天使';
				break;
			case 'god':
				return '上帝';
				break;
			case 'beijing':
				return '北京';
				break;
			case 'shanghai':
				return '上海';
				break;
			case 'shenzhen':
				return '深圳';
				break;
			case 'guangzhou':
				return '广州';
				break;
			case 'man':
				return '男';
				break;
			case 'woman':
				return '女';
				break;
			case 'unsure':
				return '保密';
				break;
			case 'zero':
				return '未提交';
				break;
			case 'one':
				return '竞标中';
				break;
			case 'two':
				return '已抢';
				break;
			case 'three':
				return '未付款';
				break;
			case 'four':
				return '抢单失败';
				break;
			case 'five':
				return '未发货';
				break;
			case 'six':
				return '已发货';
				break;
			case 'seven':
				return '已完成';
				break;
			case 'eight':
				return '已评价';
				break;
			case 'nine':
				return '上帝中止(1)';
				break;
			case 'ten':
				return '上帝中止(2)';
				break;
			case 'eleven':
				return '天使中止(3)';
				break;
			case 'twelve':
				return '平台中止(4)';
				break;
			case 'a':
				return '无需分配';
				break;
			case 'b':
				return '待支付';
				break;
			case 'c':
				return '已支付';
				break;
			case 'd':
				return '待退款';
				break;
			case 'e':
				return '已付款';
				break;
			case 'wine':
				return '酒类';
				break;
			case 'anli':
				return '保健品';
				break;
			case 'fmcg':
				return '快消品';
				break;
			default:
				return $enName;
				break;
		}
	}
 ?>