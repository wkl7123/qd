<?php 
function dersa($data)
{
	$privateKeyFilePath = $_SERVER['DOCUMENT_ROOT'].'/../rsa/private_key.txt';  
	extension_loaded('openssl') or die('php需要openssl扩展支持');  
	file_exists($privateKeyFilePath) or die('密钥或者公钥的文件路径不正确');
	$privateKey = openssl_pkey_get_private(file_get_contents($privateKeyFilePath));  
	$decrypted="";
	openssl_private_decrypt(base64_decode($data),$decrypted,$privateKey);
	return $decrypted;
}
 ?>