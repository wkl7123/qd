<?php 
function dorsa($data)
{
	$publicKeyFilePath = $_SERVER['DOCUMENT_ROOT'].'/../rsa/public_key.txt';  
	extension_loaded('openssl') or die('php需要openssl扩展支持');  
	file_exists($publicKeyFilePath) or die('密钥或者公钥的文件路径不正确');
	$publicKey  = openssl_pkey_get_public(file_get_contents($publicKeyFilePath));  
	$encrypted="";
	openssl_public_encrypt($data,$encrypted,$publicKey);
	$encrypted = base64_encode($encrypted);
	return $encrypted;
}
 ?>