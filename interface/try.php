<?php 
	ini_set('display_errors', 'On');
	error_reporting(E_ALL &~ E_NOTICE);
	$uid=$_REQUEST['uid'];
	$message=$_REQUEST['message'];
	require_once('../php/pushMessage.php');
	try{
		pushMessage($uid,$message);
		echo json_encode(array("code"=>200,"message"=>"success"));
	}catch(Exception $e){
		die(json_encode(array("code"=>101,"error"=>$e->getMessage())));
	}

?>
