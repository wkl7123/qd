<?php
try{
    header("Content-Type: text/html; charset=UTF-8");
    $n=$_REQUEST['n'];
    $uid=$_REQUEST['uid'];
    $comment=$_REQUEST['comment'];
    $comment=urldecode($comment);
    $uidx=$_REQUEST['uidx'];
    include '../php/connect_mysql.php';
    $connection->query("SET NAMES UTF8");
    $result=$connection->query("select comments from anecdote_guide where n=$n");
    $row=$result->fetch_assoc();
    $comments=json_decode($row['comments'],true);
    $comments[]=array('uid'=>$uid,'uidx'=>$uidx,'comment'=>$comment,'time'=>time());
    $comments=json_encode($comments,JSON_UNESCAPED_UNICODE);
    $result=$connection->query("update anecdote_guide set comments='$comments' where n=$n");
    if($result){
        echo json_encode(array("code"=>200,"message"=>"OK"));
    }else{
        echo json_encode(array("code"=>101,"message"=>"Database Error"));
    }
}catch(Exception $e){
    echo json_encode(array("code"=>101,"message"=>$e->getMessage()));
}
?>
