<?php 
header("Content-Type:text/html;charset=UTF-8");
try{
	$time=$_REQUEST['time'];
	$num=$_REQUEST['num'];
	$num=($num=="")?5:$num;
	include '../php/connect_mysql.php';
	$connection->query("SET NAMES 'UTF8'");
	$sql="";
	if(is_null($time)){
		$sql="select n,a.uid,nickname,portrait,user_type,unix_timestamp(time) as time,text,photo1,photo2,photo3,photo4,photo5,photo6,n_favorite,favorites,n_comment,comments from anecdote_guide as a join (select uid,nickname,portrait from user) as u on u.uid=a.uid order by time desc limit $num";
	}else{
		$sql="select n,a.uid,nickname,portrait,user_type,unix_timestamp(time) as time,text,photo1,photo2,photo3,photo4,photo5,photo6,n_favorite,favorites,n_comment,comments from anecdote_guide as a join (select uid,nickname,portrait from user) as u on u.uid=a.uid where time<'$time' order by time desc limit $num";
	}
	$result=$connection->query($sql);
	$answer=array();
	while($row=$result->fetch_assoc()){
		// echo "<p>OK</p> ";
		$n         =$row['n'];
		$uid       =$row['uid'];
		$nickname  =$row['nickname'];
		$portrait  =$row['portrait'];
		$user_type =$row['user_type'];
		$time      =$row['time'];
		$text      =$row['text'];
		$photo1    =$row['photo1'];
		$photo2    =$row['photo2'];
		$photo3    =$row['photo3'];
		$photo4    =$row['photo4'];
		$photo5    =$row['photo5'];
		$photo6    =$row['photo6'];
		$n_favorite=$row['n_favorite'];
		$favorites =$row['favorites'];
		$n_comment =$row['n_comment'];
		$comments  =$row['comments'];
		$favo=json_decode($favorites,true);
		$comm=json_decode($comments,true);
		//for favo
		$n1=count($favo);
		$uids=array();
		for($i=0;$i<$n1;$i++){
			$uids[]=$favo[$i]["uid"];
		}
		$uids='("'.join('","',$uids).'")';
		$sql="select uid,nickname,portrait from user where uid in $uids";
		// echo "<p>$sql</p>";
		$result1=$connection->query($sql);
		while($row=$result1->fetch_assoc()){
			for($i=0;$i<$n1;$i++){
				if($favo[$i]['uid']===$row['uid']){
					$favo[$i]['nickname']=$row['nickname'];
					$favo[$i]['portrait']=$row['portrait'];
					break;
				}
			}
		}
		// for comments
		$n2=count($comm);
		$uids=array();
		$uidsx=array();
		for($i=0;$i<$n2;$i++){
			$uids[] =$comm[$i]["uid"];
			$uidsx[]=$comm[$i]["uidx"];
		}
		$uids='("'.join('","',$uids).'")';
		$uidsx='("'.join('","',$uidsx).'")';	
		$sql="select uid,nickname,portrait from user where uid in $uids or uid in $uidsx";
		// echo $sql;
		$result2=$connection->query($sql);
		while($row=$result2->fetch_assoc()){
			for($i=0;$i<$n2;$i++){
				if($comm[$i]['uid']==$row['uid']){
					$comm[$i]['nickname']=$row['nickname'];
					$comm[$i]['portrait']=$row['portrait'];
					break;
				}
			}
			for($i=0;$i<$n2;$i++){
				if($comm[$i]['uidx']==$row['uid']){
					$comm[$i]['nicknamex']=$row['nickname'];
					$comm[$i]['portraitx']=$row['portrait'];
					break;
				}
			}
		}
		// echo json_encode($comm);
		$arr=array("n"=>$n,"uid"=>$uid,"nickname"=>$nickname,"portrait"=>$portrait,"user_type"=>$user_type,"time"=>$time,"text"=>$text,"photo1"=>$photo1,"photo2"=>$photo2,"photo3"=>$photo3,"photo4"=>$photo4,"photo5"=>$photo5,"photo6"=>$photo6,"n_favorite"=>$n_favorite,"n_comment"=>$n_comment,"likes"=>$favo,"comments"=>$comm);
		$answer[]=$arr;
	}
	$foo=array("code"=>200,"result"=>$answer);
	echo json_encode($foo,JSON_UNESCAPED_UNICODE);
}catch(Exception $e){
	$foo=array("code"=>"101","message"=>$e->getMessage());
	die(json_encode($foo));
}

 ?>