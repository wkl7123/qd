<!-- cookie -->
<?php
// error_reporting(E_ERROR| E_PARSE);
// session_start();
$nickname=$_COOKIE["nickname"];
$password=$_COOKIE["password"];
$nickname_session=$_SESSION['nickname'];
$password_session=$_SESSION['password'];
if ($nickname != null && $password != null) {
  include 'php/connect_mysql.php';
  $question="select password from admin where nickname="."'".$nickname."'".";";
  $result=$connection->query($question);
  if ($result) {
      $row=$result->fetch_assoc();
      if ($row['password']===$password) {
        $_SESSION['nickname']=$nickname;
        $_SESSION['password']=$password;
      }
      else{
        header('Location: signin.php');
      }
  }
  $connection->close();
}
elseif($nickname_session != null && $password_session != null){
  include 'php/connect_mysql.php';
  $question="select password from admin where nickname="."'".$nickname_session."'".";";
  $result=$connection->query($question);
  if ($result) {
      $row=$result->fetch_assoc();
      if ($row['password']!==$password_session) {
        header('Location: signin.php');
      }
  }
  $connection->close();
}
else{
  header('Location: signin.php');
}
?>

<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" >
  <script type="text/javascript" src="js/jquery-2.1.1.min.js"></script>
  <link href="css/lrtk.css" rel="stylesheet" type="text/css" >
  <title>趣订</title>
  <script>
    function jumpTo(target)
    {
      document.getElementById('main-area').src=target;
    }
  </script>
  <script type="text/javascript">
    function adapt(){
      // var wh=$(window).height();
      var wh=document.body.clientHeight;
      // alert("wh="+wh);
      // $(".nav,.main,#juren").css("height",wh-100);
      $(".nav_menu-item").css("margin-top",(wh-280)/5);
      $(".main").css("width",document.body.clientWidth-200);
      var cw=document.body.clientWidth;
      if(cw<800){
        $("#welcome").css("display","none");
      }else{
        var cw=document.body.clientWidth;
        $("#welcome").css({"display":"inline-block","margin-right":(cw-900)/2});
      }
    }
    window.onresize=function(){adapt();}
  </script>
</head>
<body onload="jumpTo('html/order_management.html');adapt();">
<div class="banner">
  <img src="photos/logo_350.png" style="height:100px;height:80px;margin-top:10px;">
  <div class="banner-button-wrap"><a href='php/signout.php' class='banner-button'>退出登录</a></div>
  <div class="banner-button-wrap"><a href='php/changepd.php' class='banner-button'>修改密码</a></div>
  <!-- <div class="welcome"><span>Welcome, admin!</span></div> -->
  <img src="photos/welcome.png" id="welcome" style="height:100px;float:right;margin-right:0px;">
</div>
<div class="nav">
  <ul class="nav_menu">
    <!-- <div style="height:77px"></div> -->
    <li class="nav_menu-item"><span>订单管理</span>
    <ul class="nav_submenu">
        <li class="nav_submenu-item"><span onclick="jumpTo('html/order_management.html')">订单管理</span></li>
    </ul>
    </li>

    <li class="nav_menu-item"><span>用户管理</span>
      <ul class="nav_submenu">
        <li class="nav_submenu-item"><span onclick="jumpTo('html/user_management_god.html')">上帝</span></li>
        <li class="nav_submenu-item"><span onclick="jumpTo('html/user_management_angel.html')">天使</span></li>
      </ul>
    </li>
    <li class="nav_menu-item"><span>建议管理</span></a>
      <ul class="nav_submenu">
        <li class="nav_submenu-item"><span  onclick="jumpTo('html/advice_feedback.html')">建议反馈</span></li>
      </ul>
    </li>
    <li class="nav_menu-item"><span>趣乐园</span>
      <ul class="nav_submenu">
        <li class="nav_submenu-item"><span onclick="jumpTo('html/anecdote_announcement.html')">趣闻公告</span></li>
        <li class="nav_submenu-item"><span onclick="jumpTo('html/anecdote_guide.html')">趣闻指南</span></li>
      </ul>
    </li>
    <div style="height:1px"></div>
  </ul>
</div>

<div class="main">
  <iframe src="html/main_area.html" width="100%" height="100%" name="main-area" id="main-area" frameborder="0"></iframe>
</div>
<!-- <img src="photos/juren.png" id="juren" style="position:absolute;height:85.3%;z-index:-1;left:100px;margin-top:0px;"> -->
</body>
</html> 
